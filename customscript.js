
            var layoutData = {
							domainName: 'document360.io',
							projectVersionId: '7124c795-b84f-4a3b-bd0f-96c4798e14e6',
							algoliaSearchKey: 'OWE5YmEzODBkNDcxZDhkMDQwYjI0NDlkY2IwYmJiYzY1Y2YzOGZjN2VjNTJmY2ZlM2ExZTdmMDc2NzBlYjk4YmZpbHRlcnM9Tk9UJTIwaXNEZWxldGVkJTNBdHJ1ZSUyMEFORCUyMGlzRHJhZnQlM0FmYWxzZSUyMEFORCUyMGV4Y2x1ZGUlM0FmYWxzZSUyMEFORCUyMGlzSGlkZGVuJTNBZmFsc2UlMjBBTkQlMjBOT1QlMjBpc0NhdGVnb3J5SGlkZGVuJTNBdHJ1ZSZyZXN0cmljdEluZGljZXM9MDM1ODJiYzMtMWVjZC00OTY2LTlmNDktMGVkZjQwODEzNWM4JnZhbGlkVW50aWw9MTY0MjgzOTg0Mw==',
							algoliaSearchAttachmentsKey: 'MGEyYjcwNzgyZmVhOTg2MDYwMmFmMzM3NmFmMDM0ZDIyNTgxYWE4ZjRkNTQ4ZWMxODZlZDY1NzY3ZDNlZjY0YWZpbHRlcnM9Tk9UJTIwaXNEZWxldGVkJTNBdHJ1ZSUyMEFORCUyMGlzRHJhZnQlM0FmYWxzZSUyMEFORCUyMGV4Y2x1ZGUlM0FmYWxzZSUyMEFORCUyMGlzSGlkZGVuJTNBZmFsc2UlMjBBTkQlMjBOT1QlMjBpc0NhdGVnb3J5SGlkZGVuJTNBdHJ1ZSZyZXN0cmljdEluZGljZXM9ODYwZjlmODgtNDEyZS00NTcwLTgyMjItZDViZjJmNGI3ZGQxJnZhbGlkVW50aWw9MTY0MjgzOTg0Mw==',
							algoliaAppId: 'JX9O5RE9SU',
							projectId: '860f9f88-412e-4570-8222-d5bf2f4b7dd1',
							versionSlug: 'v3',
							mainVersion: 'True' === 'True',
							cdn: 'cdn.document360.io/static',
							languageVersionId: '03582bc3-1ecd-4966-9f49-0edf408135c8',
							isDefaultLanguage: 'True' === 'True',
							langCode: 'en',
							viewAllResults: 'View all results for',
							userName: '',
							emailId: '',
							hideUserFeedbackNameEmail: 'False' === 'True',
							enableSearchHighlight: true,
							hostingPath: 'docs',
							projectDomain: 'docs.document360.com',
							apiUrl: 'https://api.document360.io',
							showAttachmentsTabInSearch: false,
							enableSearchAttachmentsFeature: false,
							articles: 'Articles',
							attachments: 'Attachments'
					};

//	Auth0 signup code
var randomString = function (length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}

function GenerateLink() {
	return "https://document360.io/signup/";
};

function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	} else {
		return false;
	}
}

function AssignSingupLink() {
	var url = GenerateLink() + "?signup=true";

	$('.signup_link').click(function (e) {
		var email = $(this).siblings();
		var sEmail = $.trim(email.val());
		var surl = url + '&emailId=' + sEmail;

		if (sEmail.length == 0 || !(validateEmail(sEmail))) {
			$(email).addClass("error");
			e.preventDefault();
			$(this).parent().offsetParent().addClass('wrong-entry');
			setTimeout(function () {
				$('.getstarted_signup').removeClass('wrong-entry');
			}, 500);
		}

		validCompanyEmail(sEmail).done(function (result) {
			if (result == "true") {
				$.post("https://secure.biztalk360.com/api/document360/create-user", {
					"Email": sEmail,
					"SignupStage": 1
				}, function (result) {
					//$("span").html(result);
				});
				window.open(surl, '_blank');
			} else {
				$(email).addClass("error");
				$('.gmailID').show();
				e.preventDefault();
				$('.getstarted_signup').addClass('wrong-entry');
				setTimeout(function () {
					$('.getstarted_signup').removeClass('wrong-entry');
				}, 500);
			}
		});

		$(email).keypress(function () {
			$(this).removeClass('error');
			$('.gmailID').hide();
			$('.getstarted_signup').removeClass('wrong-entry');
		});

	});

	var signupButtons = $('.signup_link');

	$.each(signupButtons, function (index, button) {
		if ($(button).is("a")) {
			button.setAttribute("href", url);
			button.setAttribute('target', '_blank');
			button.onclick = AssignSingupLink;
		} else {
			var linkButton = $(button).find('a');
			if (linkButton != null && linkButton.length) {
				linkButton[0].setAttribute("href", url);
				linkButton[0].setAttribute('target', '_blank');
				linkButton[0].onclick = AssignSingupLink;
			}
		}
	});
};

function AssignLoginLink() {
	var url = GenerateLink();
	var signupButtons = $('.login_link');

	$.each(signupButtons, function (index, button) {
		if ($(button).is("a")) {
			button.setAttribute("href", url);
			button.setAttribute('target', '_blank');
			button.onclick = AssignLoginLink;
		} else {
			var linkButton = $(button).find('a');
			if (linkButton != null && linkButton.length) {
				linkButton[0].setAttribute("href", url);
				linkButton[0].setAttribute('target', '_blank');
				linkButton[0].onclick = AssignLoginLink;
			}
		}
	});
};

function RegenLinks() {
	AssignSingupLink();
	AssignLoginLink();
}

function validCompanyEmail(email) {
	return $.get("https://secure.biztalk360.com/api/kovai/is-allowed-email?productName=5&email=" + email).promise();
}

$(document).ready(function () {
	RegenLinks();
});




function initFreshChat() {
	window.fcWidget.init({
		"config": {
			"headerProperty": {
				"hideChatButton": true
			}
		},
		token: "716d2e19-cc39-400a-aa87-6e2e0c619e39",
		host: "https://wchat.freshchat.com"
	});
}

function initialize(i, t) {
	var e;
	i.getElementById(t) ? initFreshChat() : ((e = i.createElement("script")).id = t, e.async = !0, e.src = "https://wchat.in.freshchat.com/js/widget.js", e.onload = initFreshChat, i.head.appendChild(e))
}

function initiateCall() {
	initialize(document, "freshchat-js-sdk")
}
window.addEventListener ? window.addEventListener("load", initiateCall, !1) : window.attachEvent("load", initiateCall, !1);

let heading = $(".landing_top_inner.center h1").text().replace('Document360', '<span>Document360</span>');
$('.landing_top_inner.center h1').html(heading);
let link = $(".landing_top_inner.center .header-link").text().replace('Documentation', 'Get started');
$('.landing_top_inner.center .header-link').html(link);


$(document).ready(function () {
	$('.btn-video-close').click(function () {
		$(".feature-video-popup").hide();
		$(".feature-video-popup iframe").each(function () {
			var src = $(this).attr('src');
			$(this).attr('src', src);
		});
		$('body').removeClass('popup-open');
	});
	$('.feature-popup-image').click(function () {
		$('body').addClass('popup-open');
		var video = $(this).attr("data-video");
		$(".feature-video-popup").hide();
		$("#" + video).show();
	});
});

$('.videoplay').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 5000,
	fade: true,
	cssEase: 'linear',
	infinite: true,
	arrows: true,
	dots: false,
});